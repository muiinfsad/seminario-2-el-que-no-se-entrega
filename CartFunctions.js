var modulo2 = require("./dbConnection.js");

exports.addToCart = function (item, cart) {
	modulo2.checkStock(item).then(() => {
		cart.push(item);
        console.log("Added");
        console.log(cart);
	}, () => {
        console.log("There is no stock available");
    });
}

exports.removeFromCart = function (itemId, cart) {
    var removedItem = null;
    for (var itemIndex = 0; itemIndex < cart.length; itemIndex++){
        if (cart[itemIndex].id == itemId)
            removedItem = cart.splice(itemIndex, 1);
    }
    console.log("Removed");
    console.log(removedItem);
}
